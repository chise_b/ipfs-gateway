package com.ipfs.gateway.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.ipfs.gateway.IntegrationTest;
import com.ipfs.gateway.domain.Bucket;
import com.ipfs.gateway.repository.BucketRepository;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link BucketResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class BucketResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/buckets";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private BucketRepository bucketRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restBucketMockMvc;

    private Bucket bucket;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Bucket createEntity(EntityManager em) {
        Bucket bucket = new Bucket().name(DEFAULT_NAME);
        return bucket;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Bucket createUpdatedEntity(EntityManager em) {
        Bucket bucket = new Bucket().name(UPDATED_NAME);
        return bucket;
    }

    @BeforeEach
    public void initTest() {
        bucket = createEntity(em);
    }

    @Test
    @Transactional
    void createBucket() throws Exception {
        int databaseSizeBeforeCreate = bucketRepository.findAll().size();
        // Create the Bucket
        restBucketMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(bucket)))
            .andExpect(status().isCreated());

        // Validate the Bucket in the database
        List<Bucket> bucketList = bucketRepository.findAll();
        assertThat(bucketList).hasSize(databaseSizeBeforeCreate + 1);
        Bucket testBucket = bucketList.get(bucketList.size() - 1);
        assertThat(testBucket.getName()).isEqualTo(DEFAULT_NAME);
    }

    @Test
    @Transactional
    void createBucketWithExistingId() throws Exception {
        // Create the Bucket with an existing ID
        bucket.setId(1L);

        int databaseSizeBeforeCreate = bucketRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restBucketMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(bucket)))
            .andExpect(status().isBadRequest());

        // Validate the Bucket in the database
        List<Bucket> bucketList = bucketRepository.findAll();
        assertThat(bucketList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllBuckets() throws Exception {
        // Initialize the database
        bucketRepository.saveAndFlush(bucket);

        // Get all the bucketList
        restBucketMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(bucket.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)));
    }

    @Test
    @Transactional
    void getBucket() throws Exception {
        // Initialize the database
        bucketRepository.saveAndFlush(bucket);

        // Get the bucket
        restBucketMockMvc
            .perform(get(ENTITY_API_URL_ID, bucket.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(bucket.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME));
    }

    @Test
    @Transactional
    void getNonExistingBucket() throws Exception {
        // Get the bucket
        restBucketMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewBucket() throws Exception {
        // Initialize the database
        bucketRepository.saveAndFlush(bucket);

        int databaseSizeBeforeUpdate = bucketRepository.findAll().size();

        // Update the bucket
        Bucket updatedBucket = bucketRepository.findById(bucket.getId()).get();
        // Disconnect from session so that the updates on updatedBucket are not directly saved in db
        em.detach(updatedBucket);
        updatedBucket.name(UPDATED_NAME);

        restBucketMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedBucket.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedBucket))
            )
            .andExpect(status().isOk());

        // Validate the Bucket in the database
        List<Bucket> bucketList = bucketRepository.findAll();
        assertThat(bucketList).hasSize(databaseSizeBeforeUpdate);
        Bucket testBucket = bucketList.get(bucketList.size() - 1);
        assertThat(testBucket.getName()).isEqualTo(UPDATED_NAME);
    }

    @Test
    @Transactional
    void putNonExistingBucket() throws Exception {
        int databaseSizeBeforeUpdate = bucketRepository.findAll().size();
        bucket.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restBucketMockMvc
            .perform(
                put(ENTITY_API_URL_ID, bucket.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(bucket))
            )
            .andExpect(status().isBadRequest());

        // Validate the Bucket in the database
        List<Bucket> bucketList = bucketRepository.findAll();
        assertThat(bucketList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchBucket() throws Exception {
        int databaseSizeBeforeUpdate = bucketRepository.findAll().size();
        bucket.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restBucketMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(bucket))
            )
            .andExpect(status().isBadRequest());

        // Validate the Bucket in the database
        List<Bucket> bucketList = bucketRepository.findAll();
        assertThat(bucketList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamBucket() throws Exception {
        int databaseSizeBeforeUpdate = bucketRepository.findAll().size();
        bucket.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restBucketMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(bucket)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Bucket in the database
        List<Bucket> bucketList = bucketRepository.findAll();
        assertThat(bucketList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateBucketWithPatch() throws Exception {
        // Initialize the database
        bucketRepository.saveAndFlush(bucket);

        int databaseSizeBeforeUpdate = bucketRepository.findAll().size();

        // Update the bucket using partial update
        Bucket partialUpdatedBucket = new Bucket();
        partialUpdatedBucket.setId(bucket.getId());

        partialUpdatedBucket.name(UPDATED_NAME);

        restBucketMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedBucket.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedBucket))
            )
            .andExpect(status().isOk());

        // Validate the Bucket in the database
        List<Bucket> bucketList = bucketRepository.findAll();
        assertThat(bucketList).hasSize(databaseSizeBeforeUpdate);
        Bucket testBucket = bucketList.get(bucketList.size() - 1);
        assertThat(testBucket.getName()).isEqualTo(UPDATED_NAME);
    }

    @Test
    @Transactional
    void fullUpdateBucketWithPatch() throws Exception {
        // Initialize the database
        bucketRepository.saveAndFlush(bucket);

        int databaseSizeBeforeUpdate = bucketRepository.findAll().size();

        // Update the bucket using partial update
        Bucket partialUpdatedBucket = new Bucket();
        partialUpdatedBucket.setId(bucket.getId());

        partialUpdatedBucket.name(UPDATED_NAME);

        restBucketMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedBucket.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedBucket))
            )
            .andExpect(status().isOk());

        // Validate the Bucket in the database
        List<Bucket> bucketList = bucketRepository.findAll();
        assertThat(bucketList).hasSize(databaseSizeBeforeUpdate);
        Bucket testBucket = bucketList.get(bucketList.size() - 1);
        assertThat(testBucket.getName()).isEqualTo(UPDATED_NAME);
    }

    @Test
    @Transactional
    void patchNonExistingBucket() throws Exception {
        int databaseSizeBeforeUpdate = bucketRepository.findAll().size();
        bucket.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restBucketMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, bucket.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(bucket))
            )
            .andExpect(status().isBadRequest());

        // Validate the Bucket in the database
        List<Bucket> bucketList = bucketRepository.findAll();
        assertThat(bucketList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchBucket() throws Exception {
        int databaseSizeBeforeUpdate = bucketRepository.findAll().size();
        bucket.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restBucketMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(bucket))
            )
            .andExpect(status().isBadRequest());

        // Validate the Bucket in the database
        List<Bucket> bucketList = bucketRepository.findAll();
        assertThat(bucketList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamBucket() throws Exception {
        int databaseSizeBeforeUpdate = bucketRepository.findAll().size();
        bucket.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restBucketMockMvc
            .perform(patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(bucket)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Bucket in the database
        List<Bucket> bucketList = bucketRepository.findAll();
        assertThat(bucketList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteBucket() throws Exception {
        // Initialize the database
        bucketRepository.saveAndFlush(bucket);

        int databaseSizeBeforeDelete = bucketRepository.findAll().size();

        // Delete the bucket
        restBucketMockMvc
            .perform(delete(ENTITY_API_URL_ID, bucket.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Bucket> bucketList = bucketRepository.findAll();
        assertThat(bucketList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
