import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import Bucket from './bucket';
import BucketDetail from './bucket-detail';
import BucketUpdate from './bucket-update';
import BucketDeleteDialog from './bucket-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={BucketUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={BucketUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={BucketDetail} />
      <ErrorBoundaryRoute path={match.url} component={Bucket} />
    </Switch>
    <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={BucketDeleteDialog} />
  </>
);

export default Routes;
