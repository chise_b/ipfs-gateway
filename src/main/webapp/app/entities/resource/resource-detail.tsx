import React, { useEffect } from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { getEntity } from './resource.reducer';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { useAppDispatch, useAppSelector } from 'app/config/store';

export const ResourceDetail = (props: RouteComponentProps<{ id: string }>) => {
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(getEntity(props.match.params.id));
  }, []);

  const resourceEntity = useAppSelector(state => state.resource.entity);
  return (
    <Row>
      <Col md="8">
        <h2 data-cy="resourceDetailsHeading">
          <Translate contentKey="jhipsterApp.resource.detail.title">Resource</Translate>
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="id">
              <Translate contentKey="global.field.id">ID</Translate>
            </span>
          </dt>
          <dd>{resourceEntity.id}</dd>
          <dt>
            <span id="key">
              <Translate contentKey="jhipsterApp.resource.key">Key</Translate>
            </span>
          </dt>
          <dd>{resourceEntity.key}</dd>
          <dt>
            <span id="cid">
              <Translate contentKey="jhipsterApp.resource.cid">Cid</Translate>
            </span>
          </dt>
          <dd>{resourceEntity.cid}</dd>
          <dt>
            <span id="alias">
              <Translate contentKey="jhipsterApp.resource.alias">Alias</Translate>
            </span>
          </dt>
          <dd>{resourceEntity.alias}</dd>
          <dt>
            <span id="isDirectory">
              <Translate contentKey="jhipsterApp.resource.isDirectory">Is Directory</Translate>
            </span>
          </dt>
          <dd>{resourceEntity.isDirectory ? 'true' : 'false'}</dd>
          <dt>
            <Translate contentKey="jhipsterApp.resource.bucket">Bucket</Translate>
          </dt>
          <dd>{resourceEntity.bucket ? resourceEntity.bucket.name : ''}</dd>
        </dl>
        <Button tag={Link} to="/resource" replace color="info" data-cy="entityDetailsBackButton">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/resource/${resourceEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

export default ResourceDetail;
