import React, { useState, useEffect } from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, FormText, Label } from 'reactstrap';
import { isNumber, Translate, translate, ValidatedField, ValidatedForm } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IBucket } from 'app/shared/model/bucket.model';
import { getEntities as getBuckets } from 'app/entities/bucket/bucket.reducer';
import { getEntity, updateEntity, createEntity, reset } from './resource.reducer';
import { IResource } from 'app/shared/model/resource.model';
import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';
import { useAppDispatch, useAppSelector } from 'app/config/store';

export const ResourceUpdate = (props: RouteComponentProps<{ id: string }>) => {
  const dispatch = useAppDispatch();

  const [selectedFile, setSelectedFile] = useState(null);
  const [selectedFileName, setSelectedFileName] = useState('');
  const [isNew] = useState(!props.match.params || !props.match.params.id);

  const buckets = useAppSelector(state => state.bucket.entities);
  const resourceEntity = useAppSelector(state => state.resource.entity);
  const loading = useAppSelector(state => state.resource.loading);
  const updating = useAppSelector(state => state.resource.updating);
  const updateSuccess = useAppSelector(state => state.resource.updateSuccess);

  const handleClose = () => {
    props.history.push('/resource' + props.location.search);
  };

  useEffect(() => {
    if (isNew) {
      dispatch(reset());
    } else {
      dispatch(getEntity(props.match.params.id));
    }

    dispatch(getBuckets({}));
  }, []);

  useEffect(() => {
    if (updateSuccess) {
      handleClose();
    }
  }, [updateSuccess]);

  const fileChangedHandler = e => {
    e.preventDefault();
    const file = e.target.files[0];
    setSelectedFile(file);
    setSelectedFileName(file.name);
  };

  const saveEntity = values => {
    let fileForm = new FormData();
    if (selectedFile) {
      fileForm.append('file', selectedFile);
    }
    const entity = {
      fileForm: fileForm,
      ...resourceEntity,
      ...values,
      bucket: buckets.find(it => it.id.toString() === values.bucketId.toString()),
    };

    if (isNew) {
      dispatch(createEntity(entity));
    } else {
      dispatch(updateEntity(entity));
    }
  };

  const defaultValues = () =>
    isNew
      ? null
      : {
          ...resourceEntity,
          bucketId: resourceEntity?.bucket?.id,
        };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="jhipsterApp.resource.home.createOrEditLabel" data-cy="ResourceCreateUpdateHeading">
            <Translate contentKey="jhipsterApp.resource.home.createOrEditLabel">Create or edit a Resource</Translate>
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <ValidatedForm defaultValues={defaultValues()} onSubmit={saveEntity}>
              {!isNew ? (
                <ValidatedField
                  name="id"
                  required
                  readOnly
                  id="resource-id"
                  label={translate('global.field.id')}
                  validate={{ required: true }}
                />
              ) : null}
              <ValidatedField
                label={translate('jhipsterApp.resource.key')}
                id="resource-key"
                name="key"
                data-cy="key"
                type="text"
                validate={{
                  required: { value: true, message: translate('entity.validation.required') },
                }}
              />
              <ValidatedField
                label={translate('jhipsterApp.resource.file')}
                id="resource-file"
                name="file"
                type="file"
                validate={{
                  required: { value: isNew, message: translate('entity.validation.required') },
                }}
                onChange={fileChangedHandler}
              />
              <Label htmlFor="resource-file">Selected file: {selectedFileName ? selectedFileName : 'No file chosen'}</Label>
              <ValidatedField
                readOnly
                label={translate('jhipsterApp.resource.cid')}
                id="resource-cid"
                name="cid"
                data-cy="cid"
                type="text"
              />
              <ValidatedField
                label={translate('jhipsterApp.resource.alias')}
                id="resource-alias"
                name="alias"
                data-cy="alias"
                type="text"
              />
              <ValidatedField
                label={translate('jhipsterApp.resource.isDirectory')}
                id="resource-isDirectory"
                name="isDirectory"
                data-cy="isDirectory"
                check
                type="checkbox"
              />
              <ValidatedField
                id="resource-bucket"
                name="bucketId"
                data-cy="bucket"
                label={translate('jhipsterApp.resource.bucket')}
                type="select"
              >
                <option value="" key="0" />
                {buckets
                  ? buckets.map(otherEntity => (
                      <option value={otherEntity.id} key={otherEntity.id}>
                        {otherEntity.name}
                      </option>
                    ))
                  : null}
              </ValidatedField>
              <Button tag={Link} id="cancel-save" data-cy="entityCreateCancelButton" to="/resource" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" data-cy="entityCreateSaveButton" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </ValidatedForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

export default ResourceUpdate;
