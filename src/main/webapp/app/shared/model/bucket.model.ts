import { IResource } from 'app/shared/model/resource.model';

export interface IBucket {
  id?: number;
  name?: string | null;
  resources?: IResource[] | null;
}

export const defaultValue: Readonly<IBucket> = {};
