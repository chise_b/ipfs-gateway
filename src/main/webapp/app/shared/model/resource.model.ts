import { IBucket } from 'app/shared/model/bucket.model';

export interface IResource {
  id?: number;
  key?: string;
  cid?: string | null;
  alias?: string | null;
  isDirectory?: boolean | null;
  bucket?: IBucket | null;
  fileForm?: FormData | null;
}

export const defaultValue: Readonly<IResource> = {
  isDirectory: false,
};
