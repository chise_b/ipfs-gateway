/**
 * View Models used by Spring MVC REST controllers.
 */
package com.ipfs.gateway.web.rest.vm;
