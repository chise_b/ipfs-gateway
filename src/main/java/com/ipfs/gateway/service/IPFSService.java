package com.ipfs.gateway.service;

import com.ipfs.gateway.config.ApplicationProperties;
import io.ipfs.api.IPFS;
import io.ipfs.api.MerkleNode;
import io.ipfs.api.NamedStreamable;
import io.ipfs.multihash.Multihash;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class IPFSService {

    private IPFS ipfs;

    private String ipfsNode;

    @Autowired
    private ApplicationProperties applicationProperties;

    @PostConstruct
    private void initService() {
        ipfsNode = applicationProperties.getIpfsNode();
        ipfs = new IPFS(ipfsNode);
    }

    public InputStream downloadFileByCID(String cid) throws IOException {
        Multihash hash = Multihash.fromBase58(cid);
        return ipfs.catStream(hash);
    }

    public void removePin(String cid) throws IOException {
        Multihash hash = Multihash.fromBase58(cid);
        ipfs.pin.rm(hash);
    }

    public String uploadFile(MultipartFile file) throws IOException {
        List<MerkleNode> nodes = ipfs.add(new NamedStreamable.ByteArrayWrapper(file.getOriginalFilename(), file.getBytes()));
        return nodes.get(0).hash.toString();
    }
}
