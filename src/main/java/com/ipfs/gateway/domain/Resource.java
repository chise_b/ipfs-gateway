package com.ipfs.gateway.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Resource.
 */
@Entity
@Table(name = "resource")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Resource implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "key", nullable = false)
    private String key;

    @Column(name = "cid")
    private String cid;

    @Column(name = "alias")
    private String alias;

    @Column(name = "is_directory")
    private Boolean isDirectory;

    @ManyToOne
    @JsonIgnoreProperties(value = { "resources" }, allowSetters = true)
    private Bucket bucket;

    @ManyToOne
    @JsonIgnoreProperties(value = { "resources" }, allowSetters = true)
    private User user;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Resource id(Long id) {
        this.id = id;
        return this;
    }

    public String getKey() {
        return this.key;
    }

    public Resource key(String key) {
        this.key = key;
        return this;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public Resource alias(String alias) {
        this.alias = alias;
        return this;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getCid() {
        return this.cid;
    }

    public Resource cid(String cid) {
        this.cid = cid;
        return this;
    }

    public void setCid(String cid) {
        this.cid = cid;
    }

    public Boolean getIsDirectory() {
        return this.isDirectory;
    }

    public Resource isDirectory(Boolean isDirectory) {
        this.isDirectory = isDirectory;
        return this;
    }

    public void setIsDirectory(Boolean isDirectory) {
        this.isDirectory = isDirectory;
    }

    public Bucket getBucket() {
        return this.bucket;
    }

    public Resource bucket(Bucket bucket) {
        this.setBucket(bucket);
        return this;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setBucket(Bucket bucket) {
        this.bucket = bucket;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Resource)) {
            return false;
        }
        return id != null && id.equals(((Resource) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Resource{" +
            "id=" + getId() +
            ", key='" + getKey() + "'" +
            ", cid='" + getCid() + "'" +
            ", isDirectory='" + getIsDirectory() + "'" +
            "}";
    }
}
