package com.ipfs.gateway.repository;

import com.ipfs.gateway.domain.Resource;
import com.ipfs.gateway.domain.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the Resource entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ResourceRepository extends JpaRepository<Resource, Long> {
    Page<Resource> findByUser(User user, Pageable pageable);
}
